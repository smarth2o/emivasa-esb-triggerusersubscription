package ro.setmobile.esb.emivasa.endpoints;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Consume;


@Path("/TriggerUserSubscription")
public class TriggerUserRegistration {
    @GET
    @Path("/triggerUserSubscription")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDevicesInfo(@QueryParam("user_id") String user_id, @QueryParam("data_start") String data_start, @QueryParam("data_stop") String data_stop) {
        return null;
    }
}
