package ro.setmobile.esb.emivasa.remotews;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/TriggerUserSubscription")
public interface TriggerUserSubscription {


	//@GET
	//@Path("/getDevicesInfo")
	//@Produces(MediaType.APPLICATION_JSON)
	//public String getDevicesInfoGet(@QueryParam("user_id") String user_id);
	
	@POST
	@Path("/triggerUserSubscription")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getDevicesInfo(String json);//@QueryParam("user_id") String user_id);
}
