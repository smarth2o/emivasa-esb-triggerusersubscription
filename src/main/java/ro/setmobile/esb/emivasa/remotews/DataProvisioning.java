package ro.setmobile.esb.emivasa.remotews;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/DataProvisioningExample")
public interface DataProvisioning {

	@GET
	@Path("/dataProvisioningExample")
	@Produces(MediaType.APPLICATION_JSON)
	public String getDevicesInfo(@QueryParam("user_id") String user_id,
			@QueryParam("data_start") String data_start,
			@QueryParam("data_stop") String data_stop);
}
