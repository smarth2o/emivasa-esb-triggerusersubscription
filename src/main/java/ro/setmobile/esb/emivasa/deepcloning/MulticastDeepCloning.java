package ro.setmobile.esb.emivasa.deepcloning;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class MulticastDeepCloning implements Processor {
 

	@Override
	public void process(Exchange exchange) throws Exception {
		String body = exchange.getIn().getBody(String.class);
        String clone = new String(body);
        exchange.getIn().setBody(clone);		
	}
}