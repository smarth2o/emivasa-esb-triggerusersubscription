package ro.setmobile.esb.emivasa.dbbeans;

import java.util.Date;
import java.util.Map;

import org.apache.camel.Body;
import org.apache.commons.lang.time.DateFormatUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.istack.logging.Logger;

public class LogDbBean {
	Logger log= Logger.getLogger(LogDbBean.class);
	public String toSql(@Body String body) {		
		String inputBody = body;
		Map<String,Object> inputBodyMapper;
		StringBuilder logInsertQuery=new StringBuilder("insert into emivasa_log (timestamp, user_id,readings_inserted,message_payload) values (");
		Date date=new Date();
		String dateString=DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss");
		logInsertQuery.append("'"+dateString+"',");
		ObjectMapper mapper = new ObjectMapper();
		try{
			inputBodyMapper = mapper.readValue(inputBody, Map.class);
			if(inputBodyMapper.get("user_id")!=null && inputBodyMapper.get("readings_inserted")!=null){				
				String userId=inputBodyMapper.get("user_id").toString();
				Integer readingsInserted=Integer.parseInt(inputBodyMapper.get("readings_inserted").toString());
				logInsertQuery.append("\'"+userId+"\',"+readingsInserted+",");

			}else{
				logInsertQuery.append("null,null,");
			}
		}catch(Exception e){
			logInsertQuery.append("null,null,");
		}
		logInsertQuery.append("'"+inputBody+"')");
		log.info("Query String "+logInsertQuery.toString());
		return logInsertQuery.toString();
	}
}
