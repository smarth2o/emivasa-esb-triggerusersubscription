package ro.setmobile.esb.emivasa.bundleactivator;

import java.util.logging.Level;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.sun.istack.logging.Logger;

public class EmivasaBundleActivator implements BundleActivator{

	@Override
	public void start(BundleContext arg0) throws Exception {
		Logger.getLogger(EmivasaBundleActivator.class).log(Level.INFO, "START BUNDLE FROM BUNDLE ACTIVATOR");
		try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Logger.getLogger(EmivasaBundleActivator.class).log(Level.INFO, "OK DRIVER");
        } catch (Exception ex) {
        	Logger.getLogger(EmivasaBundleActivator.class).log(Level.INFO, "NOT OK DRIVER");
         System.out.println(ex.getMessage());
            throw new IllegalStateException(
                    "Could not load JDBC driver class", ex);
            
        }
		
	}

	@Override
	public void stop(BundleContext arg0) throws Exception {

		
	}

}
