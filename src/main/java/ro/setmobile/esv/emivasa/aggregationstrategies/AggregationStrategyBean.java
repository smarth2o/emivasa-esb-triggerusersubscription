
package ro.setmobile.esv.emivasa.aggregationstrategies;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class AggregationStrategyBean implements AggregationStrategy {

	static Logger log = Logger.getLogger(AggregationStrategyBean.class);

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		if (newExchange == null) {
			return oldExchange;
		}
		String oldBody = oldExchange.getIn().getBody(String.class);
		String newBody = newExchange.getIn().getBody(String.class);
		String body = newBody;
		String user_id = "";

		log.info("Header camel query :"
				+ oldExchange.getIn().getHeader("CamelHttpQuery"));

		Map<String, String> result = null;
		try {
			result = splitQuery(oldExchange.getIn().getHeader("CamelHttpQuery")
					.toString());
			for (Map.Entry<String, String> entry : result.entrySet()) {
				log.info(entry.getKey() + "/" + entry.getValue());
				if (entry.getKey().equals("user_id")) {
					user_id = entry.getValue();
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		Map<String, Object> jsonNewMapper;
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsonNewMapper = mapper.readValue(newBody, Map.class);
		} catch (Exception e) {
			return newExchange;
		}
		if (jsonNewMapper.get("email") != null || jsonNewMapper.containsKey("email")) {
			jsonNewMapper.remove("email");
		}
		if (jsonNewMapper.get("user_id") != null) {
			jsonNewMapper.remove("user_id");
		}
		if (result != null && result.get("username") != null) {
			jsonNewMapper.put("username", result.get("username"));
		}
		if (result != null && result.get("email") != null) {
			jsonNewMapper.put("email", result.get("email"));
		}
		if (result != null && result.get("user_id") != null) {
			jsonNewMapper.put("user_id", result.get("user_id"));
		}
		try {
			body = mapper.writeValueAsString(jsonNewMapper);
		} catch (Exception e) {
			e.printStackTrace();
		}

		oldExchange.getIn().setHeaders(new HashMap<String, Object>());
		oldExchange.getIn().setHeader(Exchange.HTTP_METHOD, "POST");
		oldExchange.getIn()
				.setHeader(Exchange.CONTENT_TYPE, "application/json");
		oldExchange.getIn().setHeader("operationName", "getDevicesInfo");

		oldExchange.getIn().setBody(body);
		return oldExchange;
	}

	public static Map<String, String> splitQuery(String url)
			throws UnsupportedEncodingException {
		Map<String, String> query_pairs = new LinkedHashMap<String, String>();
		String query = url;
		String[] pairs = query.split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		return query_pairs;
	}
}
